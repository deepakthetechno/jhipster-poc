package com.capgemini.cgdevacclerator.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;

import com.capgemini.cgdevacclerator.IntegrationTest;
import com.capgemini.cgdevacclerator.domain.Policy;
import com.capgemini.cgdevacclerator.repository.EntityManager;
import com.capgemini.cgdevacclerator.repository.PolicyRepository;
import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link PolicyResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient(timeout = IntegrationTest.DEFAULT_ENTITY_TIMEOUT)
@WithMockUser
class PolicyResourceIT {

    private static final String DEFAULT_POLICY_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_POLICY_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_POLICY_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_POLICY_TYPE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/policies";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PolicyRepository policyRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Policy policy;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Policy createEntity(EntityManager em) {
        Policy policy = new Policy().policyNumber(DEFAULT_POLICY_NUMBER).policyType(DEFAULT_POLICY_TYPE);
        return policy;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Policy createUpdatedEntity(EntityManager em) {
        Policy policy = new Policy().policyNumber(UPDATED_POLICY_NUMBER).policyType(UPDATED_POLICY_TYPE);
        return policy;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Policy.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        policy = createEntity(em);
    }

    @Test
    void createPolicy() throws Exception {
        int databaseSizeBeforeCreate = policyRepository.findAll().collectList().block().size();
        // Create the Policy
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(policy))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Policy in the database
        List<Policy> policyList = policyRepository.findAll().collectList().block();
        assertThat(policyList).hasSize(databaseSizeBeforeCreate + 1);
        Policy testPolicy = policyList.get(policyList.size() - 1);
        assertThat(testPolicy.getPolicyNumber()).isEqualTo(DEFAULT_POLICY_NUMBER);
        assertThat(testPolicy.getPolicyType()).isEqualTo(DEFAULT_POLICY_TYPE);
    }

    @Test
    void createPolicyWithExistingId() throws Exception {
        // Create the Policy with an existing ID
        policy.setId(1L);

        int databaseSizeBeforeCreate = policyRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(policy))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Policy in the database
        List<Policy> policyList = policyRepository.findAll().collectList().block();
        assertThat(policyList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void checkPolicyNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = policyRepository.findAll().collectList().block().size();
        // set the field null
        policy.setPolicyNumber(null);

        // Create the Policy, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(policy))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<Policy> policyList = policyRepository.findAll().collectList().block();
        assertThat(policyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllPolicies() {
        // Initialize the database
        policyRepository.save(policy).block();

        // Get all the policyList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(policy.getId().intValue()))
            .jsonPath("$.[*].policyNumber")
            .value(hasItem(DEFAULT_POLICY_NUMBER))
            .jsonPath("$.[*].policyType")
            .value(hasItem(DEFAULT_POLICY_TYPE));
    }

    @Test
    void getPolicy() {
        // Initialize the database
        policyRepository.save(policy).block();

        // Get the policy
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, policy.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(policy.getId().intValue()))
            .jsonPath("$.policyNumber")
            .value(is(DEFAULT_POLICY_NUMBER))
            .jsonPath("$.policyType")
            .value(is(DEFAULT_POLICY_TYPE));
    }

    @Test
    void getNonExistingPolicy() {
        // Get the policy
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putExistingPolicy() throws Exception {
        // Initialize the database
        policyRepository.save(policy).block();

        int databaseSizeBeforeUpdate = policyRepository.findAll().collectList().block().size();

        // Update the policy
        Policy updatedPolicy = policyRepository.findById(policy.getId()).block();
        updatedPolicy.policyNumber(UPDATED_POLICY_NUMBER).policyType(UPDATED_POLICY_TYPE);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedPolicy.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedPolicy))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Policy in the database
        List<Policy> policyList = policyRepository.findAll().collectList().block();
        assertThat(policyList).hasSize(databaseSizeBeforeUpdate);
        Policy testPolicy = policyList.get(policyList.size() - 1);
        assertThat(testPolicy.getPolicyNumber()).isEqualTo(UPDATED_POLICY_NUMBER);
        assertThat(testPolicy.getPolicyType()).isEqualTo(UPDATED_POLICY_TYPE);
    }

    @Test
    void putNonExistingPolicy() throws Exception {
        int databaseSizeBeforeUpdate = policyRepository.findAll().collectList().block().size();
        policy.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, policy.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(policy))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Policy in the database
        List<Policy> policyList = policyRepository.findAll().collectList().block();
        assertThat(policyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchPolicy() throws Exception {
        int databaseSizeBeforeUpdate = policyRepository.findAll().collectList().block().size();
        policy.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(policy))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Policy in the database
        List<Policy> policyList = policyRepository.findAll().collectList().block();
        assertThat(policyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamPolicy() throws Exception {
        int databaseSizeBeforeUpdate = policyRepository.findAll().collectList().block().size();
        policy.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(policy))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Policy in the database
        List<Policy> policyList = policyRepository.findAll().collectList().block();
        assertThat(policyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdatePolicyWithPatch() throws Exception {
        // Initialize the database
        policyRepository.save(policy).block();

        int databaseSizeBeforeUpdate = policyRepository.findAll().collectList().block().size();

        // Update the policy using partial update
        Policy partialUpdatedPolicy = new Policy();
        partialUpdatedPolicy.setId(policy.getId());

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedPolicy.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedPolicy))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Policy in the database
        List<Policy> policyList = policyRepository.findAll().collectList().block();
        assertThat(policyList).hasSize(databaseSizeBeforeUpdate);
        Policy testPolicy = policyList.get(policyList.size() - 1);
        assertThat(testPolicy.getPolicyNumber()).isEqualTo(DEFAULT_POLICY_NUMBER);
        assertThat(testPolicy.getPolicyType()).isEqualTo(DEFAULT_POLICY_TYPE);
    }

    @Test
    void fullUpdatePolicyWithPatch() throws Exception {
        // Initialize the database
        policyRepository.save(policy).block();

        int databaseSizeBeforeUpdate = policyRepository.findAll().collectList().block().size();

        // Update the policy using partial update
        Policy partialUpdatedPolicy = new Policy();
        partialUpdatedPolicy.setId(policy.getId());

        partialUpdatedPolicy.policyNumber(UPDATED_POLICY_NUMBER).policyType(UPDATED_POLICY_TYPE);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedPolicy.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedPolicy))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Policy in the database
        List<Policy> policyList = policyRepository.findAll().collectList().block();
        assertThat(policyList).hasSize(databaseSizeBeforeUpdate);
        Policy testPolicy = policyList.get(policyList.size() - 1);
        assertThat(testPolicy.getPolicyNumber()).isEqualTo(UPDATED_POLICY_NUMBER);
        assertThat(testPolicy.getPolicyType()).isEqualTo(UPDATED_POLICY_TYPE);
    }

    @Test
    void patchNonExistingPolicy() throws Exception {
        int databaseSizeBeforeUpdate = policyRepository.findAll().collectList().block().size();
        policy.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, policy.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(policy))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Policy in the database
        List<Policy> policyList = policyRepository.findAll().collectList().block();
        assertThat(policyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchPolicy() throws Exception {
        int databaseSizeBeforeUpdate = policyRepository.findAll().collectList().block().size();
        policy.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(policy))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Policy in the database
        List<Policy> policyList = policyRepository.findAll().collectList().block();
        assertThat(policyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamPolicy() throws Exception {
        int databaseSizeBeforeUpdate = policyRepository.findAll().collectList().block().size();
        policy.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(policy))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Policy in the database
        List<Policy> policyList = policyRepository.findAll().collectList().block();
        assertThat(policyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deletePolicy() {
        // Initialize the database
        policyRepository.save(policy).block();

        int databaseSizeBeforeDelete = policyRepository.findAll().collectList().block().size();

        // Delete the policy
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, policy.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Policy> policyList = policyRepository.findAll().collectList().block();
        assertThat(policyList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
