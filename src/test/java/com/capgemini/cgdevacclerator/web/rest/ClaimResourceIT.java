package com.capgemini.cgdevacclerator.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;

import com.capgemini.cgdevacclerator.IntegrationTest;
import com.capgemini.cgdevacclerator.domain.Claim;
import com.capgemini.cgdevacclerator.repository.ClaimRepository;
import com.capgemini.cgdevacclerator.repository.EntityManager;
import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link ClaimResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient(timeout = IntegrationTest.DEFAULT_ENTITY_TIMEOUT)
@WithMockUser
class ClaimResourceIT {

    private static final String DEFAULT_CLAIM_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_CLAIM_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_CLAIM_STATE = "AAAAAAAAAA";
    private static final String UPDATED_CLAIM_STATE = "BBBBBBBBBB";

    private static final String DEFAULT_POLICY_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_POLICY_NUMBER = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/claims";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ClaimRepository claimRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Claim claim;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Claim createEntity(EntityManager em) {
        Claim claim = new Claim().claimNumber(DEFAULT_CLAIM_NUMBER).claimState(DEFAULT_CLAIM_STATE).policyNumber(DEFAULT_POLICY_NUMBER);
        return claim;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Claim createUpdatedEntity(EntityManager em) {
        Claim claim = new Claim().claimNumber(UPDATED_CLAIM_NUMBER).claimState(UPDATED_CLAIM_STATE).policyNumber(UPDATED_POLICY_NUMBER);
        return claim;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Claim.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        claim = createEntity(em);
    }

    @Test
    void createClaim() throws Exception {
        int databaseSizeBeforeCreate = claimRepository.findAll().collectList().block().size();
        // Create the Claim
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(claim))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Claim in the database
        List<Claim> claimList = claimRepository.findAll().collectList().block();
        assertThat(claimList).hasSize(databaseSizeBeforeCreate + 1);
        Claim testClaim = claimList.get(claimList.size() - 1);
        assertThat(testClaim.getClaimNumber()).isEqualTo(DEFAULT_CLAIM_NUMBER);
        assertThat(testClaim.getClaimState()).isEqualTo(DEFAULT_CLAIM_STATE);
        assertThat(testClaim.getPolicyNumber()).isEqualTo(DEFAULT_POLICY_NUMBER);
    }

    @Test
    void createClaimWithExistingId() throws Exception {
        // Create the Claim with an existing ID
        claim.setId(1L);

        int databaseSizeBeforeCreate = claimRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(claim))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Claim in the database
        List<Claim> claimList = claimRepository.findAll().collectList().block();
        assertThat(claimList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void checkClaimNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = claimRepository.findAll().collectList().block().size();
        // set the field null
        claim.setClaimNumber(null);

        // Create the Claim, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(claim))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<Claim> claimList = claimRepository.findAll().collectList().block();
        assertThat(claimList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkPolicyNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = claimRepository.findAll().collectList().block().size();
        // set the field null
        claim.setPolicyNumber(null);

        // Create the Claim, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(claim))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<Claim> claimList = claimRepository.findAll().collectList().block();
        assertThat(claimList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllClaims() {
        // Initialize the database
        claimRepository.save(claim).block();

        // Get all the claimList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(claim.getId().intValue()))
            .jsonPath("$.[*].claimNumber")
            .value(hasItem(DEFAULT_CLAIM_NUMBER))
            .jsonPath("$.[*].claimState")
            .value(hasItem(DEFAULT_CLAIM_STATE))
            .jsonPath("$.[*].policyNumber")
            .value(hasItem(DEFAULT_POLICY_NUMBER));
    }

    @Test
    void getClaim() {
        // Initialize the database
        claimRepository.save(claim).block();

        // Get the claim
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, claim.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(claim.getId().intValue()))
            .jsonPath("$.claimNumber")
            .value(is(DEFAULT_CLAIM_NUMBER))
            .jsonPath("$.claimState")
            .value(is(DEFAULT_CLAIM_STATE))
            .jsonPath("$.policyNumber")
            .value(is(DEFAULT_POLICY_NUMBER));
    }

    @Test
    void getNonExistingClaim() {
        // Get the claim
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putExistingClaim() throws Exception {
        // Initialize the database
        claimRepository.save(claim).block();

        int databaseSizeBeforeUpdate = claimRepository.findAll().collectList().block().size();

        // Update the claim
        Claim updatedClaim = claimRepository.findById(claim.getId()).block();
        updatedClaim.claimNumber(UPDATED_CLAIM_NUMBER).claimState(UPDATED_CLAIM_STATE).policyNumber(UPDATED_POLICY_NUMBER);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedClaim.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedClaim))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Claim in the database
        List<Claim> claimList = claimRepository.findAll().collectList().block();
        assertThat(claimList).hasSize(databaseSizeBeforeUpdate);
        Claim testClaim = claimList.get(claimList.size() - 1);
        assertThat(testClaim.getClaimNumber()).isEqualTo(UPDATED_CLAIM_NUMBER);
        assertThat(testClaim.getClaimState()).isEqualTo(UPDATED_CLAIM_STATE);
        assertThat(testClaim.getPolicyNumber()).isEqualTo(UPDATED_POLICY_NUMBER);
    }

    @Test
    void putNonExistingClaim() throws Exception {
        int databaseSizeBeforeUpdate = claimRepository.findAll().collectList().block().size();
        claim.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, claim.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(claim))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Claim in the database
        List<Claim> claimList = claimRepository.findAll().collectList().block();
        assertThat(claimList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchClaim() throws Exception {
        int databaseSizeBeforeUpdate = claimRepository.findAll().collectList().block().size();
        claim.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(claim))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Claim in the database
        List<Claim> claimList = claimRepository.findAll().collectList().block();
        assertThat(claimList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamClaim() throws Exception {
        int databaseSizeBeforeUpdate = claimRepository.findAll().collectList().block().size();
        claim.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(claim))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Claim in the database
        List<Claim> claimList = claimRepository.findAll().collectList().block();
        assertThat(claimList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateClaimWithPatch() throws Exception {
        // Initialize the database
        claimRepository.save(claim).block();

        int databaseSizeBeforeUpdate = claimRepository.findAll().collectList().block().size();

        // Update the claim using partial update
        Claim partialUpdatedClaim = new Claim();
        partialUpdatedClaim.setId(claim.getId());

        partialUpdatedClaim.claimState(UPDATED_CLAIM_STATE).policyNumber(UPDATED_POLICY_NUMBER);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedClaim.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedClaim))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Claim in the database
        List<Claim> claimList = claimRepository.findAll().collectList().block();
        assertThat(claimList).hasSize(databaseSizeBeforeUpdate);
        Claim testClaim = claimList.get(claimList.size() - 1);
        assertThat(testClaim.getClaimNumber()).isEqualTo(DEFAULT_CLAIM_NUMBER);
        assertThat(testClaim.getClaimState()).isEqualTo(UPDATED_CLAIM_STATE);
        assertThat(testClaim.getPolicyNumber()).isEqualTo(UPDATED_POLICY_NUMBER);
    }

    @Test
    void fullUpdateClaimWithPatch() throws Exception {
        // Initialize the database
        claimRepository.save(claim).block();

        int databaseSizeBeforeUpdate = claimRepository.findAll().collectList().block().size();

        // Update the claim using partial update
        Claim partialUpdatedClaim = new Claim();
        partialUpdatedClaim.setId(claim.getId());

        partialUpdatedClaim.claimNumber(UPDATED_CLAIM_NUMBER).claimState(UPDATED_CLAIM_STATE).policyNumber(UPDATED_POLICY_NUMBER);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedClaim.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedClaim))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Claim in the database
        List<Claim> claimList = claimRepository.findAll().collectList().block();
        assertThat(claimList).hasSize(databaseSizeBeforeUpdate);
        Claim testClaim = claimList.get(claimList.size() - 1);
        assertThat(testClaim.getClaimNumber()).isEqualTo(UPDATED_CLAIM_NUMBER);
        assertThat(testClaim.getClaimState()).isEqualTo(UPDATED_CLAIM_STATE);
        assertThat(testClaim.getPolicyNumber()).isEqualTo(UPDATED_POLICY_NUMBER);
    }

    @Test
    void patchNonExistingClaim() throws Exception {
        int databaseSizeBeforeUpdate = claimRepository.findAll().collectList().block().size();
        claim.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, claim.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(claim))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Claim in the database
        List<Claim> claimList = claimRepository.findAll().collectList().block();
        assertThat(claimList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchClaim() throws Exception {
        int databaseSizeBeforeUpdate = claimRepository.findAll().collectList().block().size();
        claim.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(claim))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Claim in the database
        List<Claim> claimList = claimRepository.findAll().collectList().block();
        assertThat(claimList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamClaim() throws Exception {
        int databaseSizeBeforeUpdate = claimRepository.findAll().collectList().block().size();
        claim.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(claim))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Claim in the database
        List<Claim> claimList = claimRepository.findAll().collectList().block();
        assertThat(claimList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteClaim() {
        // Initialize the database
        claimRepository.save(claim).block();

        int databaseSizeBeforeDelete = claimRepository.findAll().collectList().block().size();

        // Delete the claim
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, claim.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Claim> claimList = claimRepository.findAll().collectList().block();
        assertThat(claimList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
