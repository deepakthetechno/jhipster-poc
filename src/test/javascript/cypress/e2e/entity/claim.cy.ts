import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Claim e2e test', () => {
  const claimPageUrl = '/claim';
  const claimPageUrlPattern = new RegExp('/claim(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const claimSample = { claimNumber: 'SAS', policyNumber: 'revolutionary Managed Movies' };

  let claim;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/claims+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/claims').as('postEntityRequest');
    cy.intercept('DELETE', '/api/claims/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (claim) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/claims/${claim.id}`,
      }).then(() => {
        claim = undefined;
      });
    }
  });

  it('Claims menu should load Claims page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('claim');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Claim').should('exist');
    cy.url().should('match', claimPageUrlPattern);
  });

  describe('Claim page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(claimPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Claim page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/claim/new$'));
        cy.getEntityCreateUpdateHeading('Claim');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', claimPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/claims',
          body: claimSample,
        }).then(({ body }) => {
          claim = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/claims+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/claims?page=0&size=20>; rel="last",<http://localhost/api/claims?page=0&size=20>; rel="first"',
              },
              body: [claim],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(claimPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Claim page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('claim');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', claimPageUrlPattern);
      });

      it('edit button click should load edit Claim page and go back', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Claim');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', claimPageUrlPattern);
      });

      it('edit button click should load edit Claim page and save', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Claim');
        cy.get(entityCreateSaveButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', claimPageUrlPattern);
      });

      it('last delete button click should delete instance of Claim', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('claim').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', claimPageUrlPattern);

        claim = undefined;
      });
    });
  });

  describe('new Claim page', () => {
    beforeEach(() => {
      cy.visit(`${claimPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('Claim');
    });

    it('should create an instance of Claim', () => {
      cy.get(`[data-cy="claimNumber"]`).type('Intelligent Ford Customer').should('have.value', 'Intelligent Ford Customer');

      cy.get(`[data-cy="claimState"]`).type('digital Uganda').should('have.value', 'digital Uganda');

      cy.get(`[data-cy="policyNumber"]`).type('e-enable Concrete').should('have.value', 'e-enable Concrete');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(201);
        claim = response.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(200);
      });
      cy.url().should('match', claimPageUrlPattern);
    });
  });
});
