import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Policy e2e test', () => {
  const policyPageUrl = '/policy';
  const policyPageUrlPattern = new RegExp('/policy(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const policySample = { policyNumber: 'deposit' };

  let policy;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/policies+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/policies').as('postEntityRequest');
    cy.intercept('DELETE', '/api/policies/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (policy) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/policies/${policy.id}`,
      }).then(() => {
        policy = undefined;
      });
    }
  });

  it('Policies menu should load Policies page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('policy');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Policy').should('exist');
    cy.url().should('match', policyPageUrlPattern);
  });

  describe('Policy page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(policyPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Policy page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/policy/new$'));
        cy.getEntityCreateUpdateHeading('Policy');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', policyPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/policies',
          body: policySample,
        }).then(({ body }) => {
          policy = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/policies+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/policies?page=0&size=20>; rel="last",<http://localhost/api/policies?page=0&size=20>; rel="first"',
              },
              body: [policy],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(policyPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Policy page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('policy');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', policyPageUrlPattern);
      });

      it('edit button click should load edit Policy page and go back', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Policy');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', policyPageUrlPattern);
      });

      it('edit button click should load edit Policy page and save', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Policy');
        cy.get(entityCreateSaveButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', policyPageUrlPattern);
      });

      it('last delete button click should delete instance of Policy', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('policy').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', policyPageUrlPattern);

        policy = undefined;
      });
    });
  });

  describe('new Policy page', () => {
    beforeEach(() => {
      cy.visit(`${policyPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('Policy');
    });

    it('should create an instance of Policy', () => {
      cy.get(`[data-cy="policyNumber"]`).type('application Fish').should('have.value', 'application Fish');

      cy.get(`[data-cy="policyType"]`).type('Right-sized').should('have.value', 'Right-sized');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(201);
        policy = response.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(200);
      });
      cy.url().should('match', policyPageUrlPattern);
    });
  });
});
