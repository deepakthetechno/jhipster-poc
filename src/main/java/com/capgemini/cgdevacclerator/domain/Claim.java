package com.capgemini.cgdevacclerator.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.validation.constraints.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Claim.
 */
@Table("claim")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Claim implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @NotNull(message = "must not be null")
    @Column("claim_number")
    private String claimNumber;

    @Column("claim_state")
    private String claimState;

    @NotNull(message = "must not be null")
    @Column("policy_number")
    private String policyNumber;

    @Transient
    private Customer customer;

    @Transient
    @JsonIgnoreProperties(value = { "claims", "customer" }, allowSetters = true)
    private Policy policy;

    @Column("policy_id")
    private Long policyId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Claim id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClaimNumber() {
        return this.claimNumber;
    }

    public Claim claimNumber(String claimNumber) {
        this.setClaimNumber(claimNumber);
        return this;
    }

    public void setClaimNumber(String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public String getClaimState() {
        return this.claimState;
    }

    public Claim claimState(String claimState) {
        this.setClaimState(claimState);
        return this;
    }

    public void setClaimState(String claimState) {
        this.claimState = claimState;
    }

    public String getPolicyNumber() {
        return this.policyNumber;
    }

    public Claim policyNumber(String policyNumber) {
        this.setPolicyNumber(policyNumber);
        return this;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public Customer getCustomer() {
        return this.customer;
    }

    public void setCustomer(Customer customer) {
        if (this.customer != null) {
            this.customer.setClaim(null);
        }
        if (customer != null) {
            customer.setClaim(this);
        }
        this.customer = customer;
    }

    public Claim customer(Customer customer) {
        this.setCustomer(customer);
        return this;
    }

    public Policy getPolicy() {
        return this.policy;
    }

    public void setPolicy(Policy policy) {
        this.policy = policy;
        this.policyId = policy != null ? policy.getId() : null;
    }

    public Claim policy(Policy policy) {
        this.setPolicy(policy);
        return this;
    }

    public Long getPolicyId() {
        return this.policyId;
    }

    public void setPolicyId(Long policy) {
        this.policyId = policy;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Claim)) {
            return false;
        }
        return id != null && id.equals(((Claim) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Claim{" +
            "id=" + getId() +
            ", claimNumber='" + getClaimNumber() + "'" +
            ", claimState='" + getClaimState() + "'" +
            ", policyNumber='" + getPolicyNumber() + "'" +
            "}";
    }
}
