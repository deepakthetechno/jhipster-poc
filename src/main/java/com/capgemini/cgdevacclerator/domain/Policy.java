package com.capgemini.cgdevacclerator.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.validation.constraints.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Policy.
 */
@Table("policy")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Policy implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @NotNull(message = "must not be null")
    @Column("policy_number")
    private String policyNumber;

    @Column("policy_type")
    private String policyType;

    @Transient
    @JsonIgnoreProperties(value = { "customer", "policy" }, allowSetters = true)
    private Set<Claim> claims = new HashSet<>();

    @Transient
    private Customer customer;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Policy id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPolicyNumber() {
        return this.policyNumber;
    }

    public Policy policyNumber(String policyNumber) {
        this.setPolicyNumber(policyNumber);
        return this;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getPolicyType() {
        return this.policyType;
    }

    public Policy policyType(String policyType) {
        this.setPolicyType(policyType);
        return this;
    }

    public void setPolicyType(String policyType) {
        this.policyType = policyType;
    }

    public Set<Claim> getClaims() {
        return this.claims;
    }

    public void setClaims(Set<Claim> claims) {
        if (this.claims != null) {
            this.claims.forEach(i -> i.setPolicy(null));
        }
        if (claims != null) {
            claims.forEach(i -> i.setPolicy(this));
        }
        this.claims = claims;
    }

    public Policy claims(Set<Claim> claims) {
        this.setClaims(claims);
        return this;
    }

    public Policy addClaim(Claim claim) {
        this.claims.add(claim);
        claim.setPolicy(this);
        return this;
    }

    public Policy removeClaim(Claim claim) {
        this.claims.remove(claim);
        claim.setPolicy(null);
        return this;
    }

    public Customer getCustomer() {
        return this.customer;
    }

    public void setCustomer(Customer customer) {
        if (this.customer != null) {
            this.customer.setPolicy(null);
        }
        if (customer != null) {
            customer.setPolicy(this);
        }
        this.customer = customer;
    }

    public Policy customer(Customer customer) {
        this.setCustomer(customer);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Policy)) {
            return false;
        }
        return id != null && id.equals(((Policy) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Policy{" +
            "id=" + getId() +
            ", policyNumber='" + getPolicyNumber() + "'" +
            ", policyType='" + getPolicyType() + "'" +
            "}";
    }
}
