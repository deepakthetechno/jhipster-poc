package com.capgemini.cgdevacclerator.service.impl;

import com.capgemini.cgdevacclerator.domain.Claim;
import com.capgemini.cgdevacclerator.repository.ClaimRepository;
import com.capgemini.cgdevacclerator.service.ClaimService;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Implementation for managing {@link Claim}.
 */
@Service
@Transactional
public class ClaimServiceImpl implements ClaimService {

    private final Logger log = LoggerFactory.getLogger(ClaimServiceImpl.class);

    private final ClaimRepository claimRepository;

    public ClaimServiceImpl(ClaimRepository claimRepository) {
        this.claimRepository = claimRepository;
    }

    @Override
    public Mono<Claim> save(Claim claim) {
        log.debug("Request to save Claim : {}", claim);
        return claimRepository.save(claim);
    }

    @Override
    public Mono<Claim> update(Claim claim) {
        log.debug("Request to update Claim : {}", claim);
        return claimRepository.save(claim);
    }

    @Override
    public Mono<Claim> partialUpdate(Claim claim) {
        log.debug("Request to partially update Claim : {}", claim);

        return claimRepository
            .findById(claim.getId())
            .map(existingClaim -> {
                if (claim.getClaimNumber() != null) {
                    existingClaim.setClaimNumber(claim.getClaimNumber());
                }
                if (claim.getClaimState() != null) {
                    existingClaim.setClaimState(claim.getClaimState());
                }
                if (claim.getPolicyNumber() != null) {
                    existingClaim.setPolicyNumber(claim.getPolicyNumber());
                }

                return existingClaim;
            })
            .flatMap(claimRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Flux<Claim> findAll(Pageable pageable) {
        log.debug("Request to get all Claims");
        return claimRepository.findAllBy(pageable);
    }

    /**
     *  Get all the claims where Customer is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Flux<Claim> findAllWhereCustomerIsNull() {
        log.debug("Request to get all claims where Customer is null");
        return claimRepository.findAllWhereCustomerIsNull();
    }

    public Mono<Long> countAll() {
        return claimRepository.count();
    }

    @Override
    @Transactional(readOnly = true)
    public Mono<Claim> findOne(Long id) {
        log.debug("Request to get Claim : {}", id);
        return claimRepository.findById(id);
    }

    @Override
    public Mono<Void> delete(Long id) {
        log.debug("Request to delete Claim : {}", id);
        return claimRepository.deleteById(id);
    }
}
