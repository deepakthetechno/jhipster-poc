package com.capgemini.cgdevacclerator.service.impl;

import com.capgemini.cgdevacclerator.domain.Policy;
import com.capgemini.cgdevacclerator.repository.PolicyRepository;
import com.capgemini.cgdevacclerator.service.PolicyService;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Implementation for managing {@link Policy}.
 */
@Service
@Transactional
public class PolicyServiceImpl implements PolicyService {

    private final Logger log = LoggerFactory.getLogger(PolicyServiceImpl.class);

    private final PolicyRepository policyRepository;

    public PolicyServiceImpl(PolicyRepository policyRepository) {
        this.policyRepository = policyRepository;
    }

    @Override
    public Mono<Policy> save(Policy policy) {
        log.debug("Request to save Policy : {}", policy);
        return policyRepository.save(policy);
    }

    @Override
    public Mono<Policy> update(Policy policy) {
        log.debug("Request to update Policy : {}", policy);
        return policyRepository.save(policy);
    }

    @Override
    public Mono<Policy> partialUpdate(Policy policy) {
        log.debug("Request to partially update Policy : {}", policy);

        return policyRepository
            .findById(policy.getId())
            .map(existingPolicy -> {
                if (policy.getPolicyNumber() != null) {
                    existingPolicy.setPolicyNumber(policy.getPolicyNumber());
                }
                if (policy.getPolicyType() != null) {
                    existingPolicy.setPolicyType(policy.getPolicyType());
                }

                return existingPolicy;
            })
            .flatMap(policyRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Flux<Policy> findAll(Pageable pageable) {
        log.debug("Request to get all Policies");
        return policyRepository.findAllBy(pageable);
    }

    /**
     *  Get all the policies where Customer is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Flux<Policy> findAllWhereCustomerIsNull() {
        log.debug("Request to get all policies where Customer is null");
        return policyRepository.findAllWhereCustomerIsNull();
    }

    public Mono<Long> countAll() {
        return policyRepository.count();
    }

    @Override
    @Transactional(readOnly = true)
    public Mono<Policy> findOne(Long id) {
        log.debug("Request to get Policy : {}", id);
        return policyRepository.findById(id);
    }

    @Override
    public Mono<Void> delete(Long id) {
        log.debug("Request to delete Policy : {}", id);
        return policyRepository.deleteById(id);
    }
}
