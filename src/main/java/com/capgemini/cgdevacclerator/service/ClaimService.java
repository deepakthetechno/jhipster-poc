package com.capgemini.cgdevacclerator.service;

import com.capgemini.cgdevacclerator.domain.Claim;
import java.util.List;
import org.springframework.data.domain.Pageable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Interface for managing {@link Claim}.
 */
public interface ClaimService {
    /**
     * Save a claim.
     *
     * @param claim the entity to save.
     * @return the persisted entity.
     */
    Mono<Claim> save(Claim claim);

    /**
     * Updates a claim.
     *
     * @param claim the entity to update.
     * @return the persisted entity.
     */
    Mono<Claim> update(Claim claim);

    /**
     * Partially updates a claim.
     *
     * @param claim the entity to update partially.
     * @return the persisted entity.
     */
    Mono<Claim> partialUpdate(Claim claim);

    /**
     * Get all the claims.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Flux<Claim> findAll(Pageable pageable);
    /**
     * Get all the Claim where Customer is {@code null}.
     *
     * @return the {@link Flux} of entities.
     */
    Flux<Claim> findAllWhereCustomerIsNull();

    /**
     * Returns the number of claims available.
     * @return the number of entities in the database.
     *
     */
    Mono<Long> countAll();

    /**
     * Get the "id" claim.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Mono<Claim> findOne(Long id);

    /**
     * Delete the "id" claim.
     *
     * @param id the id of the entity.
     * @return a Mono to signal the deletion
     */
    Mono<Void> delete(Long id);
}
