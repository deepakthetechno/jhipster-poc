package com.capgemini.cgdevacclerator.service;

import com.capgemini.cgdevacclerator.domain.Policy;
import java.util.List;
import org.springframework.data.domain.Pageable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Interface for managing {@link Policy}.
 */
public interface PolicyService {
    /**
     * Save a policy.
     *
     * @param policy the entity to save.
     * @return the persisted entity.
     */
    Mono<Policy> save(Policy policy);

    /**
     * Updates a policy.
     *
     * @param policy the entity to update.
     * @return the persisted entity.
     */
    Mono<Policy> update(Policy policy);

    /**
     * Partially updates a policy.
     *
     * @param policy the entity to update partially.
     * @return the persisted entity.
     */
    Mono<Policy> partialUpdate(Policy policy);

    /**
     * Get all the policies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Flux<Policy> findAll(Pageable pageable);
    /**
     * Get all the Policy where Customer is {@code null}.
     *
     * @return the {@link Flux} of entities.
     */
    Flux<Policy> findAllWhereCustomerIsNull();

    /**
     * Returns the number of policies available.
     * @return the number of entities in the database.
     *
     */
    Mono<Long> countAll();

    /**
     * Get the "id" policy.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Mono<Policy> findOne(Long id);

    /**
     * Delete the "id" policy.
     *
     * @param id the id of the entity.
     * @return a Mono to signal the deletion
     */
    Mono<Void> delete(Long id);
}
