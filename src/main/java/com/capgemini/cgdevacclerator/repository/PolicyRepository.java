package com.capgemini.cgdevacclerator.repository;

import com.capgemini.cgdevacclerator.domain.Policy;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data R2DBC repository for the Policy entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PolicyRepository extends ReactiveCrudRepository<Policy, Long>, PolicyRepositoryInternal {
    Flux<Policy> findAllBy(Pageable pageable);

    @Query("SELECT * FROM policy entity WHERE entity.id not in (select customer_id from customer)")
    Flux<Policy> findAllWhereCustomerIsNull();

    @Override
    <S extends Policy> Mono<S> save(S entity);

    @Override
    Flux<Policy> findAll();

    @Override
    Mono<Policy> findById(Long id);

    @Override
    Mono<Void> deleteById(Long id);
}

interface PolicyRepositoryInternal {
    <S extends Policy> Mono<S> save(S entity);

    Flux<Policy> findAllBy(Pageable pageable);

    Flux<Policy> findAll();

    Mono<Policy> findById(Long id);
    // this is not supported at the moment because of https://github.com/jhipster/generator-jhipster/issues/18269
    // Flux<Policy> findAllBy(Pageable pageable, Criteria criteria);

}
