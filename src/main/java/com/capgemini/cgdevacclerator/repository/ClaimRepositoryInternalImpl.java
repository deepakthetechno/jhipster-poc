package com.capgemini.cgdevacclerator.repository;

import static org.springframework.data.relational.core.query.Criteria.where;

import com.capgemini.cgdevacclerator.domain.Claim;
import com.capgemini.cgdevacclerator.repository.rowmapper.ClaimRowMapper;
import com.capgemini.cgdevacclerator.repository.rowmapper.PolicyRowMapper;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiFunction;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.convert.R2dbcConverter;
import org.springframework.data.r2dbc.core.R2dbcEntityOperations;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.r2dbc.repository.support.SimpleR2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Comparison;
import org.springframework.data.relational.core.sql.Condition;
import org.springframework.data.relational.core.sql.Conditions;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Select;
import org.springframework.data.relational.core.sql.SelectBuilder.SelectFromAndJoinCondition;
import org.springframework.data.relational.core.sql.Table;
import org.springframework.data.relational.repository.support.MappingRelationalEntityInformation;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.r2dbc.core.RowsFetchSpec;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data R2DBC custom repository implementation for the Claim entity.
 */
@SuppressWarnings("unused")
class ClaimRepositoryInternalImpl extends SimpleR2dbcRepository<Claim, Long> implements ClaimRepositoryInternal {

    private final DatabaseClient db;
    private final R2dbcEntityTemplate r2dbcEntityTemplate;
    private final EntityManager entityManager;

    private final PolicyRowMapper policyMapper;
    private final ClaimRowMapper claimMapper;

    private static final Table entityTable = Table.aliased("claim", EntityManager.ENTITY_ALIAS);
    private static final Table policyTable = Table.aliased("policy", "policy");

    public ClaimRepositoryInternalImpl(
        R2dbcEntityTemplate template,
        EntityManager entityManager,
        PolicyRowMapper policyMapper,
        ClaimRowMapper claimMapper,
        R2dbcEntityOperations entityOperations,
        R2dbcConverter converter
    ) {
        super(
            new MappingRelationalEntityInformation(converter.getMappingContext().getRequiredPersistentEntity(Claim.class)),
            entityOperations,
            converter
        );
        this.db = template.getDatabaseClient();
        this.r2dbcEntityTemplate = template;
        this.entityManager = entityManager;
        this.policyMapper = policyMapper;
        this.claimMapper = claimMapper;
    }

    @Override
    public Flux<Claim> findAllBy(Pageable pageable) {
        return createQuery(pageable, null).all();
    }

    RowsFetchSpec<Claim> createQuery(Pageable pageable, Condition whereClause) {
        List<Expression> columns = ClaimSqlHelper.getColumns(entityTable, EntityManager.ENTITY_ALIAS);
        columns.addAll(PolicySqlHelper.getColumns(policyTable, "policy"));
        SelectFromAndJoinCondition selectFrom = Select
            .builder()
            .select(columns)
            .from(entityTable)
            .leftOuterJoin(policyTable)
            .on(Column.create("policy_id", entityTable))
            .equals(Column.create("id", policyTable));
        // we do not support Criteria here for now as of https://github.com/jhipster/generator-jhipster/issues/18269
        String select = entityManager.createSelect(selectFrom, Claim.class, pageable, whereClause);
        return db.sql(select).map(this::process);
    }

    @Override
    public Flux<Claim> findAll() {
        return findAllBy(null);
    }

    @Override
    public Mono<Claim> findById(Long id) {
        Comparison whereClause = Conditions.isEqual(entityTable.column("id"), Conditions.just(id.toString()));
        return createQuery(null, whereClause).one();
    }

    private Claim process(Row row, RowMetadata metadata) {
        Claim entity = claimMapper.apply(row, "e");
        entity.setPolicy(policyMapper.apply(row, "policy"));
        return entity;
    }

    @Override
    public <S extends Claim> Mono<S> save(S entity) {
        return super.save(entity);
    }
}
