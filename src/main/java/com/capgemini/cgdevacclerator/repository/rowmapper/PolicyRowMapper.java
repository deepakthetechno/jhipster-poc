package com.capgemini.cgdevacclerator.repository.rowmapper;

import com.capgemini.cgdevacclerator.domain.Policy;
import io.r2dbc.spi.Row;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Policy}, with proper type conversions.
 */
@Service
public class PolicyRowMapper implements BiFunction<Row, String, Policy> {

    private final ColumnConverter converter;

    public PolicyRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Policy} stored in the database.
     */
    @Override
    public Policy apply(Row row, String prefix) {
        Policy entity = new Policy();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setPolicyNumber(converter.fromRow(row, prefix + "_policy_number", String.class));
        entity.setPolicyType(converter.fromRow(row, prefix + "_policy_type", String.class));
        return entity;
    }
}
