package com.capgemini.cgdevacclerator.repository.rowmapper;

import com.capgemini.cgdevacclerator.domain.Claim;
import io.r2dbc.spi.Row;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Claim}, with proper type conversions.
 */
@Service
public class ClaimRowMapper implements BiFunction<Row, String, Claim> {

    private final ColumnConverter converter;

    public ClaimRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Claim} stored in the database.
     */
    @Override
    public Claim apply(Row row, String prefix) {
        Claim entity = new Claim();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setClaimNumber(converter.fromRow(row, prefix + "_claim_number", String.class));
        entity.setClaimState(converter.fromRow(row, prefix + "_claim_state", String.class));
        entity.setPolicyNumber(converter.fromRow(row, prefix + "_policy_number", String.class));
        entity.setPolicyId(converter.fromRow(row, prefix + "_policy_id", Long.class));
        return entity;
    }
}
