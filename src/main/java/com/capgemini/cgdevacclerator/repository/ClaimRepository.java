package com.capgemini.cgdevacclerator.repository;

import com.capgemini.cgdevacclerator.domain.Claim;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data R2DBC repository for the Claim entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClaimRepository extends ReactiveCrudRepository<Claim, Long>, ClaimRepositoryInternal {
    Flux<Claim> findAllBy(Pageable pageable);

    @Query("SELECT * FROM claim entity WHERE entity.id not in (select customer_id from customer)")
    Flux<Claim> findAllWhereCustomerIsNull();

    @Query("SELECT * FROM claim entity WHERE entity.policy_id = :id")
    Flux<Claim> findByPolicy(Long id);

    @Query("SELECT * FROM claim entity WHERE entity.policy_id IS NULL")
    Flux<Claim> findAllWherePolicyIsNull();

    @Override
    <S extends Claim> Mono<S> save(S entity);

    @Override
    Flux<Claim> findAll();

    @Override
    Mono<Claim> findById(Long id);

    @Override
    Mono<Void> deleteById(Long id);
}

interface ClaimRepositoryInternal {
    <S extends Claim> Mono<S> save(S entity);

    Flux<Claim> findAllBy(Pageable pageable);

    Flux<Claim> findAll();

    Mono<Claim> findById(Long id);
    // this is not supported at the moment because of https://github.com/jhipster/generator-jhipster/issues/18269
    // Flux<Claim> findAllBy(Pageable pageable, Criteria criteria);

}
