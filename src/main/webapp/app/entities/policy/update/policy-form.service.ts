import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IPolicy, NewPolicy } from '../policy.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IPolicy for edit and NewPolicyFormGroupInput for create.
 */
type PolicyFormGroupInput = IPolicy | PartialWithRequiredKeyOf<NewPolicy>;

type PolicyFormDefaults = Pick<NewPolicy, 'id'>;

type PolicyFormGroupContent = {
  id: FormControl<IPolicy['id'] | NewPolicy['id']>;
  policyNumber: FormControl<IPolicy['policyNumber']>;
  policyType: FormControl<IPolicy['policyType']>;
};

export type PolicyFormGroup = FormGroup<PolicyFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class PolicyFormService {
  createPolicyFormGroup(policy: PolicyFormGroupInput = { id: null }): PolicyFormGroup {
    const policyRawValue = {
      ...this.getFormDefaults(),
      ...policy,
    };
    return new FormGroup<PolicyFormGroupContent>({
      id: new FormControl(
        { value: policyRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      policyNumber: new FormControl(policyRawValue.policyNumber, {
        validators: [Validators.required],
      }),
      policyType: new FormControl(policyRawValue.policyType),
    });
  }

  getPolicy(form: PolicyFormGroup): IPolicy | NewPolicy {
    return form.getRawValue() as IPolicy | NewPolicy;
  }

  resetForm(form: PolicyFormGroup, policy: PolicyFormGroupInput): void {
    const policyRawValue = { ...this.getFormDefaults(), ...policy };
    form.reset(
      {
        ...policyRawValue,
        id: { value: policyRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): PolicyFormDefaults {
    return {
      id: null,
    };
  }
}
