import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../policy.test-samples';

import { PolicyFormService } from './policy-form.service';

describe('Policy Form Service', () => {
  let service: PolicyFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PolicyFormService);
  });

  describe('Service methods', () => {
    describe('createPolicyFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createPolicyFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            policyNumber: expect.any(Object),
            policyType: expect.any(Object),
          })
        );
      });

      it('passing IPolicy should create a new form with FormGroup', () => {
        const formGroup = service.createPolicyFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            policyNumber: expect.any(Object),
            policyType: expect.any(Object),
          })
        );
      });
    });

    describe('getPolicy', () => {
      it('should return NewPolicy for default Policy initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createPolicyFormGroup(sampleWithNewData);

        const policy = service.getPolicy(formGroup) as any;

        expect(policy).toMatchObject(sampleWithNewData);
      });

      it('should return NewPolicy for empty Policy initial value', () => {
        const formGroup = service.createPolicyFormGroup();

        const policy = service.getPolicy(formGroup) as any;

        expect(policy).toMatchObject({});
      });

      it('should return IPolicy', () => {
        const formGroup = service.createPolicyFormGroup(sampleWithRequiredData);

        const policy = service.getPolicy(formGroup) as any;

        expect(policy).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IPolicy should not enable id FormControl', () => {
        const formGroup = service.createPolicyFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewPolicy should disable id FormControl', () => {
        const formGroup = service.createPolicyFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
