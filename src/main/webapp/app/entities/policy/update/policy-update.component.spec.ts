import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { PolicyFormService } from './policy-form.service';
import { PolicyService } from '../service/policy.service';
import { IPolicy } from '../policy.model';

import { PolicyUpdateComponent } from './policy-update.component';

describe('Policy Management Update Component', () => {
  let comp: PolicyUpdateComponent;
  let fixture: ComponentFixture<PolicyUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let policyFormService: PolicyFormService;
  let policyService: PolicyService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [PolicyUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(PolicyUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PolicyUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    policyFormService = TestBed.inject(PolicyFormService);
    policyService = TestBed.inject(PolicyService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const policy: IPolicy = { id: 456 };

      activatedRoute.data = of({ policy });
      comp.ngOnInit();

      expect(comp.policy).toEqual(policy);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IPolicy>>();
      const policy = { id: 123 };
      jest.spyOn(policyFormService, 'getPolicy').mockReturnValue(policy);
      jest.spyOn(policyService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ policy });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: policy }));
      saveSubject.complete();

      // THEN
      expect(policyFormService.getPolicy).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(policyService.update).toHaveBeenCalledWith(expect.objectContaining(policy));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IPolicy>>();
      const policy = { id: 123 };
      jest.spyOn(policyFormService, 'getPolicy').mockReturnValue({ id: null });
      jest.spyOn(policyService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ policy: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: policy }));
      saveSubject.complete();

      // THEN
      expect(policyFormService.getPolicy).toHaveBeenCalled();
      expect(policyService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IPolicy>>();
      const policy = { id: 123 };
      jest.spyOn(policyService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ policy });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(policyService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
