import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { PolicyFormService, PolicyFormGroup } from './policy-form.service';
import { IPolicy } from '../policy.model';
import { PolicyService } from '../service/policy.service';

@Component({
  selector: 'jhi-policy-update',
  templateUrl: './policy-update.component.html',
})
export class PolicyUpdateComponent implements OnInit {
  isSaving = false;
  policy: IPolicy | null = null;

  editForm: PolicyFormGroup = this.policyFormService.createPolicyFormGroup();

  constructor(
    protected policyService: PolicyService,
    protected policyFormService: PolicyFormService,
    protected activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ policy }) => {
      this.policy = policy;
      if (policy) {
        this.updateForm(policy);
      }
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const policy = this.policyFormService.getPolicy(this.editForm);
    if (policy.id !== null) {
      this.subscribeToSaveResponse(this.policyService.update(policy));
    } else {
      this.subscribeToSaveResponse(this.policyService.create(policy));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPolicy>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(policy: IPolicy): void {
    this.policy = policy;
    this.policyFormService.resetForm(this.editForm, policy);
  }
}
