import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IPolicy } from '../policy.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../policy.test-samples';

import { PolicyService } from './policy.service';

const requireRestSample: IPolicy = {
  ...sampleWithRequiredData,
};

describe('Policy Service', () => {
  let service: PolicyService;
  let httpMock: HttpTestingController;
  let expectedResult: IPolicy | IPolicy[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(PolicyService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Policy', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const policy = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(policy).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Policy', () => {
      const policy = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(policy).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Policy', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Policy', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Policy', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addPolicyToCollectionIfMissing', () => {
      it('should add a Policy to an empty array', () => {
        const policy: IPolicy = sampleWithRequiredData;
        expectedResult = service.addPolicyToCollectionIfMissing([], policy);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(policy);
      });

      it('should not add a Policy to an array that contains it', () => {
        const policy: IPolicy = sampleWithRequiredData;
        const policyCollection: IPolicy[] = [
          {
            ...policy,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addPolicyToCollectionIfMissing(policyCollection, policy);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Policy to an array that doesn't contain it", () => {
        const policy: IPolicy = sampleWithRequiredData;
        const policyCollection: IPolicy[] = [sampleWithPartialData];
        expectedResult = service.addPolicyToCollectionIfMissing(policyCollection, policy);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(policy);
      });

      it('should add only unique Policy to an array', () => {
        const policyArray: IPolicy[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const policyCollection: IPolicy[] = [sampleWithRequiredData];
        expectedResult = service.addPolicyToCollectionIfMissing(policyCollection, ...policyArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const policy: IPolicy = sampleWithRequiredData;
        const policy2: IPolicy = sampleWithPartialData;
        expectedResult = service.addPolicyToCollectionIfMissing([], policy, policy2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(policy);
        expect(expectedResult).toContain(policy2);
      });

      it('should accept null and undefined values', () => {
        const policy: IPolicy = sampleWithRequiredData;
        expectedResult = service.addPolicyToCollectionIfMissing([], null, policy, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(policy);
      });

      it('should return initial array if no Policy is added', () => {
        const policyCollection: IPolicy[] = [sampleWithRequiredData];
        expectedResult = service.addPolicyToCollectionIfMissing(policyCollection, undefined, null);
        expect(expectedResult).toEqual(policyCollection);
      });
    });

    describe('comparePolicy', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.comparePolicy(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.comparePolicy(entity1, entity2);
        const compareResult2 = service.comparePolicy(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.comparePolicy(entity1, entity2);
        const compareResult2 = service.comparePolicy(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.comparePolicy(entity1, entity2);
        const compareResult2 = service.comparePolicy(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
