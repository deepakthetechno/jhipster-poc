import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPolicy, NewPolicy } from '../policy.model';

export type PartialUpdatePolicy = Partial<IPolicy> & Pick<IPolicy, 'id'>;

export type EntityResponseType = HttpResponse<IPolicy>;
export type EntityArrayResponseType = HttpResponse<IPolicy[]>;

@Injectable({ providedIn: 'root' })
export class PolicyService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/policies');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(policy: NewPolicy): Observable<EntityResponseType> {
    return this.http.post<IPolicy>(this.resourceUrl, policy, { observe: 'response' });
  }

  update(policy: IPolicy): Observable<EntityResponseType> {
    return this.http.put<IPolicy>(`${this.resourceUrl}/${this.getPolicyIdentifier(policy)}`, policy, { observe: 'response' });
  }

  partialUpdate(policy: PartialUpdatePolicy): Observable<EntityResponseType> {
    return this.http.patch<IPolicy>(`${this.resourceUrl}/${this.getPolicyIdentifier(policy)}`, policy, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPolicy>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPolicy[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getPolicyIdentifier(policy: Pick<IPolicy, 'id'>): number {
    return policy.id;
  }

  comparePolicy(o1: Pick<IPolicy, 'id'> | null, o2: Pick<IPolicy, 'id'> | null): boolean {
    return o1 && o2 ? this.getPolicyIdentifier(o1) === this.getPolicyIdentifier(o2) : o1 === o2;
  }

  addPolicyToCollectionIfMissing<Type extends Pick<IPolicy, 'id'>>(
    policyCollection: Type[],
    ...policiesToCheck: (Type | null | undefined)[]
  ): Type[] {
    const policies: Type[] = policiesToCheck.filter(isPresent);
    if (policies.length > 0) {
      const policyCollectionIdentifiers = policyCollection.map(policyItem => this.getPolicyIdentifier(policyItem)!);
      const policiesToAdd = policies.filter(policyItem => {
        const policyIdentifier = this.getPolicyIdentifier(policyItem);
        if (policyCollectionIdentifiers.includes(policyIdentifier)) {
          return false;
        }
        policyCollectionIdentifiers.push(policyIdentifier);
        return true;
      });
      return [...policiesToAdd, ...policyCollection];
    }
    return policyCollection;
  }
}
