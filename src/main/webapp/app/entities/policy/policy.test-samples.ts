import { IPolicy, NewPolicy } from './policy.model';

export const sampleWithRequiredData: IPolicy = {
  id: 62250,
  policyNumber: 'Plastic',
};

export const sampleWithPartialData: IPolicy = {
  id: 32889,
  policyNumber: 'violet',
};

export const sampleWithFullData: IPolicy = {
  id: 66672,
  policyNumber: 'Avon',
  policyType: 'invoice',
};

export const sampleWithNewData: NewPolicy = {
  policyNumber: 'Pakistan Soft',
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
