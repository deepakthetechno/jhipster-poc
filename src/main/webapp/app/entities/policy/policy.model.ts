export interface IPolicy {
  id: number;
  policyNumber?: string | null;
  policyType?: string | null;
}

export type NewPolicy = Omit<IPolicy, 'id'> & { id: null };
