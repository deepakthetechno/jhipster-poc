import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IClaim, NewClaim } from '../claim.model';

export type PartialUpdateClaim = Partial<IClaim> & Pick<IClaim, 'id'>;

export type EntityResponseType = HttpResponse<IClaim>;
export type EntityArrayResponseType = HttpResponse<IClaim[]>;

@Injectable({ providedIn: 'root' })
export class ClaimService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/claims');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(claim: NewClaim): Observable<EntityResponseType> {
    return this.http.post<IClaim>(this.resourceUrl, claim, { observe: 'response' });
  }

  update(claim: IClaim): Observable<EntityResponseType> {
    return this.http.put<IClaim>(`${this.resourceUrl}/${this.getClaimIdentifier(claim)}`, claim, { observe: 'response' });
  }

  partialUpdate(claim: PartialUpdateClaim): Observable<EntityResponseType> {
    return this.http.patch<IClaim>(`${this.resourceUrl}/${this.getClaimIdentifier(claim)}`, claim, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IClaim>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IClaim[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getClaimIdentifier(claim: Pick<IClaim, 'id'>): number {
    return claim.id;
  }

  compareClaim(o1: Pick<IClaim, 'id'> | null, o2: Pick<IClaim, 'id'> | null): boolean {
    return o1 && o2 ? this.getClaimIdentifier(o1) === this.getClaimIdentifier(o2) : o1 === o2;
  }

  addClaimToCollectionIfMissing<Type extends Pick<IClaim, 'id'>>(
    claimCollection: Type[],
    ...claimsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const claims: Type[] = claimsToCheck.filter(isPresent);
    if (claims.length > 0) {
      const claimCollectionIdentifiers = claimCollection.map(claimItem => this.getClaimIdentifier(claimItem)!);
      const claimsToAdd = claims.filter(claimItem => {
        const claimIdentifier = this.getClaimIdentifier(claimItem);
        if (claimCollectionIdentifiers.includes(claimIdentifier)) {
          return false;
        }
        claimCollectionIdentifiers.push(claimIdentifier);
        return true;
      });
      return [...claimsToAdd, ...claimCollection];
    }
    return claimCollection;
  }
}
