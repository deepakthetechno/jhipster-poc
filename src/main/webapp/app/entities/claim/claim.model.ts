import { IPolicy } from 'app/entities/policy/policy.model';

export interface IClaim {
  id: number;
  claimNumber?: string | null;
  claimState?: string | null;
  policyNumber?: string | null;
  policy?: Pick<IPolicy, 'id'> | null;
}

export type NewClaim = Omit<IClaim, 'id'> & { id: null };
