import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ClaimFormService, ClaimFormGroup } from './claim-form.service';
import { IClaim } from '../claim.model';
import { ClaimService } from '../service/claim.service';
import { IPolicy } from 'app/entities/policy/policy.model';
import { PolicyService } from 'app/entities/policy/service/policy.service';

@Component({
  selector: 'jhi-claim-update',
  templateUrl: './claim-update.component.html',
})
export class ClaimUpdateComponent implements OnInit {
  isSaving = false;
  claim: IClaim | null = null;

  policiesSharedCollection: IPolicy[] = [];

  editForm: ClaimFormGroup = this.claimFormService.createClaimFormGroup();

  constructor(
    protected claimService: ClaimService,
    protected claimFormService: ClaimFormService,
    protected policyService: PolicyService,
    protected activatedRoute: ActivatedRoute
  ) {}

  comparePolicy = (o1: IPolicy | null, o2: IPolicy | null): boolean => this.policyService.comparePolicy(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ claim }) => {
      this.claim = claim;
      if (claim) {
        this.updateForm(claim);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const claim = this.claimFormService.getClaim(this.editForm);
    if (claim.id !== null) {
      this.subscribeToSaveResponse(this.claimService.update(claim));
    } else {
      this.subscribeToSaveResponse(this.claimService.create(claim));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IClaim>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(claim: IClaim): void {
    this.claim = claim;
    this.claimFormService.resetForm(this.editForm, claim);

    this.policiesSharedCollection = this.policyService.addPolicyToCollectionIfMissing<IPolicy>(this.policiesSharedCollection, claim.policy);
  }

  protected loadRelationshipsOptions(): void {
    this.policyService
      .query()
      .pipe(map((res: HttpResponse<IPolicy[]>) => res.body ?? []))
      .pipe(map((policies: IPolicy[]) => this.policyService.addPolicyToCollectionIfMissing<IPolicy>(policies, this.claim?.policy)))
      .subscribe((policies: IPolicy[]) => (this.policiesSharedCollection = policies));
  }
}
