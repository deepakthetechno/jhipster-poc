import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { ClaimFormService } from './claim-form.service';
import { ClaimService } from '../service/claim.service';
import { IClaim } from '../claim.model';
import { IPolicy } from 'app/entities/policy/policy.model';
import { PolicyService } from 'app/entities/policy/service/policy.service';

import { ClaimUpdateComponent } from './claim-update.component';

describe('Claim Management Update Component', () => {
  let comp: ClaimUpdateComponent;
  let fixture: ComponentFixture<ClaimUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let claimFormService: ClaimFormService;
  let claimService: ClaimService;
  let policyService: PolicyService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [ClaimUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(ClaimUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ClaimUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    claimFormService = TestBed.inject(ClaimFormService);
    claimService = TestBed.inject(ClaimService);
    policyService = TestBed.inject(PolicyService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Policy query and add missing value', () => {
      const claim: IClaim = { id: 456 };
      const policy: IPolicy = { id: 41095 };
      claim.policy = policy;

      const policyCollection: IPolicy[] = [{ id: 44804 }];
      jest.spyOn(policyService, 'query').mockReturnValue(of(new HttpResponse({ body: policyCollection })));
      const additionalPolicies = [policy];
      const expectedCollection: IPolicy[] = [...additionalPolicies, ...policyCollection];
      jest.spyOn(policyService, 'addPolicyToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ claim });
      comp.ngOnInit();

      expect(policyService.query).toHaveBeenCalled();
      expect(policyService.addPolicyToCollectionIfMissing).toHaveBeenCalledWith(
        policyCollection,
        ...additionalPolicies.map(expect.objectContaining)
      );
      expect(comp.policiesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const claim: IClaim = { id: 456 };
      const policy: IPolicy = { id: 94296 };
      claim.policy = policy;

      activatedRoute.data = of({ claim });
      comp.ngOnInit();

      expect(comp.policiesSharedCollection).toContain(policy);
      expect(comp.claim).toEqual(claim);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IClaim>>();
      const claim = { id: 123 };
      jest.spyOn(claimFormService, 'getClaim').mockReturnValue(claim);
      jest.spyOn(claimService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ claim });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: claim }));
      saveSubject.complete();

      // THEN
      expect(claimFormService.getClaim).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(claimService.update).toHaveBeenCalledWith(expect.objectContaining(claim));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IClaim>>();
      const claim = { id: 123 };
      jest.spyOn(claimFormService, 'getClaim').mockReturnValue({ id: null });
      jest.spyOn(claimService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ claim: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: claim }));
      saveSubject.complete();

      // THEN
      expect(claimFormService.getClaim).toHaveBeenCalled();
      expect(claimService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IClaim>>();
      const claim = { id: 123 };
      jest.spyOn(claimService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ claim });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(claimService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('comparePolicy', () => {
      it('Should forward to policyService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(policyService, 'comparePolicy');
        comp.comparePolicy(entity, entity2);
        expect(policyService.comparePolicy).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
