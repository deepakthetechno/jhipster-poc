import { IClaim, NewClaim } from './claim.model';

export const sampleWithRequiredData: IClaim = {
  id: 40158,
  claimNumber: 'generating neutral',
  policyNumber: 'black Bedfordshire',
};

export const sampleWithPartialData: IClaim = {
  id: 60889,
  claimNumber: 'Savings',
  claimState: 'Maryland',
  policyNumber: 'Customer',
};

export const sampleWithFullData: IClaim = {
  id: 74341,
  claimNumber: 'Account lavender',
  claimState: 'mission-critical leverage',
  policyNumber: 'Regional bleeding-edge',
};

export const sampleWithNewData: NewClaim = {
  claimNumber: 'Rwanda encompassing pixel',
  policyNumber: 'HTTP AGP Yen',
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
