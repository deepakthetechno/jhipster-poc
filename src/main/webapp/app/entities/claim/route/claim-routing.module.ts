import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ClaimComponent } from '../list/claim.component';
import { ClaimDetailComponent } from '../detail/claim-detail.component';
import { ClaimUpdateComponent } from '../update/claim-update.component';
import { ClaimRoutingResolveService } from './claim-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const claimRoute: Routes = [
  {
    path: '',
    component: ClaimComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ClaimDetailComponent,
    resolve: {
      claim: ClaimRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ClaimUpdateComponent,
    resolve: {
      claim: ClaimRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ClaimUpdateComponent,
    resolve: {
      claim: ClaimRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(claimRoute)],
  exports: [RouterModule],
})
export class ClaimRoutingModule {}
