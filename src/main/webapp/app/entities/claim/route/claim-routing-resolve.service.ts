import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IClaim } from '../claim.model';
import { ClaimService } from '../service/claim.service';

@Injectable({ providedIn: 'root' })
export class ClaimRoutingResolveService implements Resolve<IClaim | null> {
  constructor(protected service: ClaimService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IClaim | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((claim: HttpResponse<IClaim>) => {
          if (claim.body) {
            return of(claim.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
