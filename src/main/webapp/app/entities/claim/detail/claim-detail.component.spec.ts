import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ClaimDetailComponent } from './claim-detail.component';

describe('Claim Management Detail Component', () => {
  let comp: ClaimDetailComponent;
  let fixture: ComponentFixture<ClaimDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ClaimDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ claim: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(ClaimDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(ClaimDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load claim on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.claim).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
