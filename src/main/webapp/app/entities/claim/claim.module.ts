import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ClaimComponent } from './list/claim.component';
import { ClaimDetailComponent } from './detail/claim-detail.component';
import { ClaimUpdateComponent } from './update/claim-update.component';
import { ClaimDeleteDialogComponent } from './delete/claim-delete-dialog.component';
import { ClaimRoutingModule } from './route/claim-routing.module';

@NgModule({
  imports: [SharedModule, ClaimRoutingModule],
  declarations: [ClaimComponent, ClaimDetailComponent, ClaimUpdateComponent, ClaimDeleteDialogComponent],
})
export class ClaimModule {}
