export const EntityNavbarItems = [
  {
    name: 'Claim',
    route: 'claim',
    translationKey: 'global.menu.entities.claim',
  },
  {
    name: 'Policy',
    route: 'policy',
    translationKey: 'global.menu.entities.policy',
  },
  {
    name: 'Customer',
    route: 'customer',
    translationKey: 'global.menu.entities.customer',
  },
];
