import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'claim',
        data: { pageTitle: 'cgdevaccleratorApp.claim.home.title' },
        loadChildren: () => import('./claim/claim.module').then(m => m.ClaimModule),
      },
      {
        path: 'policy',
        data: { pageTitle: 'cgdevaccleratorApp.policy.home.title' },
        loadChildren: () => import('./policy/policy.module').then(m => m.PolicyModule),
      },
      {
        path: 'customer',
        data: { pageTitle: 'cgdevaccleratorApp.customer.home.title' },
        loadChildren: () => import('./customer/customer.module').then(m => m.CustomerModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
