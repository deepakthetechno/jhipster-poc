import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { CustomerFormService, CustomerFormGroup } from './customer-form.service';
import { ICustomer } from '../customer.model';
import { CustomerService } from '../service/customer.service';
import { IPolicy } from 'app/entities/policy/policy.model';
import { PolicyService } from 'app/entities/policy/service/policy.service';
import { IClaim } from 'app/entities/claim/claim.model';
import { ClaimService } from 'app/entities/claim/service/claim.service';

@Component({
  selector: 'jhi-customer-update',
  templateUrl: './customer-update.component.html',
})
export class CustomerUpdateComponent implements OnInit {
  isSaving = false;
  customer: ICustomer | null = null;

  policiesCollection: IPolicy[] = [];
  claimsCollection: IClaim[] = [];

  editForm: CustomerFormGroup = this.customerFormService.createCustomerFormGroup();

  constructor(
    protected customerService: CustomerService,
    protected customerFormService: CustomerFormService,
    protected policyService: PolicyService,
    protected claimService: ClaimService,
    protected activatedRoute: ActivatedRoute
  ) {}

  comparePolicy = (o1: IPolicy | null, o2: IPolicy | null): boolean => this.policyService.comparePolicy(o1, o2);

  compareClaim = (o1: IClaim | null, o2: IClaim | null): boolean => this.claimService.compareClaim(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ customer }) => {
      this.customer = customer;
      if (customer) {
        this.updateForm(customer);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const customer = this.customerFormService.getCustomer(this.editForm);
    if (customer.id !== null) {
      this.subscribeToSaveResponse(this.customerService.update(customer));
    } else {
      this.subscribeToSaveResponse(this.customerService.create(customer));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICustomer>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(customer: ICustomer): void {
    this.customer = customer;
    this.customerFormService.resetForm(this.editForm, customer);

    this.policiesCollection = this.policyService.addPolicyToCollectionIfMissing<IPolicy>(this.policiesCollection, customer.policy);
    this.claimsCollection = this.claimService.addClaimToCollectionIfMissing<IClaim>(this.claimsCollection, customer.claim);
  }

  protected loadRelationshipsOptions(): void {
    this.policyService
      .query({ filter: 'customer-is-null' })
      .pipe(map((res: HttpResponse<IPolicy[]>) => res.body ?? []))
      .pipe(map((policies: IPolicy[]) => this.policyService.addPolicyToCollectionIfMissing<IPolicy>(policies, this.customer?.policy)))
      .subscribe((policies: IPolicy[]) => (this.policiesCollection = policies));

    this.claimService
      .query({ filter: 'customer-is-null' })
      .pipe(map((res: HttpResponse<IClaim[]>) => res.body ?? []))
      .pipe(map((claims: IClaim[]) => this.claimService.addClaimToCollectionIfMissing<IClaim>(claims, this.customer?.claim)))
      .subscribe((claims: IClaim[]) => (this.claimsCollection = claims));
  }
}
