import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { CustomerFormService } from './customer-form.service';
import { CustomerService } from '../service/customer.service';
import { ICustomer } from '../customer.model';
import { IPolicy } from 'app/entities/policy/policy.model';
import { PolicyService } from 'app/entities/policy/service/policy.service';
import { IClaim } from 'app/entities/claim/claim.model';
import { ClaimService } from 'app/entities/claim/service/claim.service';

import { CustomerUpdateComponent } from './customer-update.component';

describe('Customer Management Update Component', () => {
  let comp: CustomerUpdateComponent;
  let fixture: ComponentFixture<CustomerUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let customerFormService: CustomerFormService;
  let customerService: CustomerService;
  let policyService: PolicyService;
  let claimService: ClaimService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [CustomerUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(CustomerUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CustomerUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    customerFormService = TestBed.inject(CustomerFormService);
    customerService = TestBed.inject(CustomerService);
    policyService = TestBed.inject(PolicyService);
    claimService = TestBed.inject(ClaimService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call policy query and add missing value', () => {
      const customer: ICustomer = { id: 456 };
      const policy: IPolicy = { id: 84631 };
      customer.policy = policy;

      const policyCollection: IPolicy[] = [{ id: 52917 }];
      jest.spyOn(policyService, 'query').mockReturnValue(of(new HttpResponse({ body: policyCollection })));
      const expectedCollection: IPolicy[] = [policy, ...policyCollection];
      jest.spyOn(policyService, 'addPolicyToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ customer });
      comp.ngOnInit();

      expect(policyService.query).toHaveBeenCalled();
      expect(policyService.addPolicyToCollectionIfMissing).toHaveBeenCalledWith(policyCollection, policy);
      expect(comp.policiesCollection).toEqual(expectedCollection);
    });

    it('Should call claim query and add missing value', () => {
      const customer: ICustomer = { id: 456 };
      const claim: IClaim = { id: 52160 };
      customer.claim = claim;

      const claimCollection: IClaim[] = [{ id: 33846 }];
      jest.spyOn(claimService, 'query').mockReturnValue(of(new HttpResponse({ body: claimCollection })));
      const expectedCollection: IClaim[] = [claim, ...claimCollection];
      jest.spyOn(claimService, 'addClaimToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ customer });
      comp.ngOnInit();

      expect(claimService.query).toHaveBeenCalled();
      expect(claimService.addClaimToCollectionIfMissing).toHaveBeenCalledWith(claimCollection, claim);
      expect(comp.claimsCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const customer: ICustomer = { id: 456 };
      const policy: IPolicy = { id: 95255 };
      customer.policy = policy;
      const claim: IClaim = { id: 22579 };
      customer.claim = claim;

      activatedRoute.data = of({ customer });
      comp.ngOnInit();

      expect(comp.policiesCollection).toContain(policy);
      expect(comp.claimsCollection).toContain(claim);
      expect(comp.customer).toEqual(customer);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ICustomer>>();
      const customer = { id: 123 };
      jest.spyOn(customerFormService, 'getCustomer').mockReturnValue(customer);
      jest.spyOn(customerService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ customer });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: customer }));
      saveSubject.complete();

      // THEN
      expect(customerFormService.getCustomer).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(customerService.update).toHaveBeenCalledWith(expect.objectContaining(customer));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ICustomer>>();
      const customer = { id: 123 };
      jest.spyOn(customerFormService, 'getCustomer').mockReturnValue({ id: null });
      jest.spyOn(customerService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ customer: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: customer }));
      saveSubject.complete();

      // THEN
      expect(customerFormService.getCustomer).toHaveBeenCalled();
      expect(customerService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ICustomer>>();
      const customer = { id: 123 };
      jest.spyOn(customerService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ customer });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(customerService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('comparePolicy', () => {
      it('Should forward to policyService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(policyService, 'comparePolicy');
        comp.comparePolicy(entity, entity2);
        expect(policyService.comparePolicy).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareClaim', () => {
      it('Should forward to claimService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(claimService, 'compareClaim');
        comp.compareClaim(entity, entity2);
        expect(claimService.compareClaim).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
