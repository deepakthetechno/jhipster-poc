import { IPolicy } from 'app/entities/policy/policy.model';
import { IClaim } from 'app/entities/claim/claim.model';

export interface ICustomer {
  id: number;
  firstName?: string | null;
  lastName?: string | null;
  policy?: Pick<IPolicy, 'id'> | null;
  claim?: Pick<IClaim, 'id'> | null;
}

export type NewCustomer = Omit<ICustomer, 'id'> & { id: null };
